<?php include __DIR__ . '/__connect_db.php';

$page_name = 'cart_list';

if(empty($_SESSION['cart'])){
    header('Location: product_list.php');
    exit();
}

$sids = array_keys($_SESSION['cart']);

//$sql = "SELECT * FROM `products` WHERE `sid` IN (". implode(',', $sids). ") ";
$sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s) ", implode(',', $sids));
//echo $sql;
$result = $mysqli->query($sql);
$p_data = array();

while($row=$result->fetch_assoc()){
    $row['qty'] = $_SESSION['cart'][$row['sid']];
    $p_data[$row['sid']] = $row;
}

//print_r($p_data);

?>
    <style>
        .remove-item {
            font-size: 36px;
            color: mediumvioletred;
            cursor: pointer;
        }
    </style>
<?php include __DIR__ . '/__html_head.php' ?>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>


        <div class="bs-example" data-example-id="striped-table">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>取消</th>
                    <th>封面</th>
                    <th>書名</th>
                    <th>價格</th>
                    <th>數量</th>
                    <th>小計</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($sids as $sid): ?>
                    <tr data-sid="<?= $sid ?>">

                        <td>
                            <span class="glyphicon glyphicon-remove-sign remove-item" aria-hidden="true"></span>
                        </td>
                        <td><img src="imgs/small/<?= $p_data[$sid]['book_id'] ?>.jpg" alt=""></td>
                        <td><?= $p_data[$sid]['bookname'] ?></td>
                        <td><?= $p_data[$sid]['price'] ?></td>
                        <td>
                            <select class="qty_sel" data-qty="<?= $p_data[$sid]['qty'] ?>">
                                <?php for($i=1; $i<=10; $i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                                <?php endfor; ?>
                            </select>
                            <!--?= $p_data[$sid]['qty'] ?-->

                        </td>
                        <td><?= $p_data[$sid]['qty']*$p_data[$sid]['price'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <div class="alert alert-info" role="alert"> 共計: <strong id="totalPrice">1000</strong> 台票</div>

            <?php if(! isset($_SESSION['user'])): ?>
                <a type="button" class="btn btn-default" href="login.php">結帳前請先登入</a>
            <?php else: ?>
                <a type="button" class="btn btn-danger" href="buy.php">結帳</a>
            <?php endif; ?>

        </div>

    </div>
    <script>
        $('.remove-item').click(function(){
            var sid = $(this).closest('tr').attr('data-sid');
            var tr =  $(this).closest('tr');

            $.get('add_to_cart.php', {sid:sid}, function(data){
                console.log(data);
                calTotalQty(data);
                //location.href = location.href; //刷新頁面, 第一種作法

                tr.remove(); // 移除 tr 元素, 第二種作法
                calTotal();
            }, 'json');

        });

        var qty_sel = $('.qty_sel');
        qty_sel.each(function(){
            var qty = $(this).attr('data-qty');
            $(this).val(qty);
        });

        qty_sel.change(function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');
            var qty = $(this).val();
            var price = tr.find('td').eq(3).text();

            $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                console.log(data);
                calTotalQty(data);

                tr.find('td').eq(5).text(price * qty)

                calTotal();
            }, 'json');
        });

        var calTotal = function(){
            var total = 0;
            $('tr[data-sid]').each(function(){
                total += $(this).find('td').eq(3).text() * $(this).find('.qty_sel').val();
                //console.log($(this).find('td').eq(3).text(),  $(this).find('.qty_sel').val());
            });

            $('#totalPrice').text(total);
        };

        calTotal();
    </script>
<?php include __DIR__ . '/__html_foot.php' ?>