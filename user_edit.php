<?php
$page_name = 'register';

require __DIR__. '/__connect_db.php';

if(isset($_POST['password'])){
    $password = $_POST['password'];
    $new_password = $_POST['new_password'];
    $nickname = $_POST['nickname'];
    $mobile = $_POST['mobile'];
    $address = $_POST['address'];

    $password = sha1($password);

    $sql = sprintf("SELECT * FROM `members` WHERE `email_id`='%s' AND `password`='%s'",
        $mysqli->escape_string($_SESSION['user']['email_id']),
        $mysqli->escape_string($password)
    );

    $result = $mysqli->query($sql);

    $success = $result->num_rows>0;

    if($success){
        $row = $result->fetch_assoc();
        if(empty($new_password)){
            $sql = sprintf("UPDATE `members` SET `nickname`='%s',`mobile`='%s',`address`='%s' WHERE `sid`=%s",
                    $mysqli->escape_string($nickname),
                    $mysqli->escape_string($mobile),
                    $mysqli->escape_string($address),
                    $row['sid']
                );
        } else {
            $sql = sprintf("UPDATE `members` SET `password`='%s', `nickname`='%s',`mobile`='%s',`address`='%s' WHERE `sid`=%s",
                sha1($new_password),
                $mysqli->escape_string($nickname),
                $mysqli->escape_string($mobile),
                $mysqli->escape_string($address),
                $row['sid']
                );
        }
        if($mysqli->query($sql)){
            $msg = array(
                'success' => true,
                'info' => '修改完成',
            );
            $_SESSION['user']['nickname'] = $nickname;
            $_SESSION['user']['mobile'] = $mobile;
            $_SESSION['user']['address'] = $address;


        }else{
            $msg = array(
                'success' => false,
                'info' => '錯誤, 請找開發人員',
            );
        }


    } else {
        $msg = array(
            'success' => false,
            'info' => '密碼錯誤',
        );


    }

}




?>
<?php include __DIR__ . '/__html_head.php' ?>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <?php if(isset($msg)): ?>

            <div class="alert alert-<?= $msg['success'] ? 'success' : 'danger' ?>" role="alert"><?= $msg['info'] ?></div>

        <?php endif; ?>

        <?php if(!isset($msg) or $msg['success']==false): ?>
            <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">會員資料修改</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();"> <!-- 不要讓表單送出 -->
                        <div class="form-group">
                            <label for="email_id">Email address</label>
                            <input type="email" class="form-control" id="email_id" value="<?= $_SESSION['user']['email_id'] ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="password">* Password</label> <span id="password_info" style="color:red;display:none;">密碼長度請設定大於 6 !</span>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="new_password">New password</label> <span id="password_info" style="color:red">不修改請留白</span>
                            <input type="password" class="form-control" id="new_password" name="new_password">
                        </div>
                        <div class="form-group">
                            <label for="nickname">* Nickname</label> <span id="nickname_info" style="color:red;display:none;">暱稱長度請設定大於 2 !</span>
                            <input type="text" class="form-control" id="nickname" name="nickname"  value="<?= $_SESSION['user']['nickname'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile</label>
                            <input type="text" class="form-control" id="mobile" name="mobile"  value="<?= $_SESSION['user']['mobile'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address"><?= $_SESSION['user']['address'] ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">修改</button>
                    </form>


                </div>
            </div>
        </div>

        <?php endif; ?>
    </div>
    <script>
        function checkForm(){
            var password = form1.password.value;
            var nickname = form1.nickname.value;


            var isPass = true;

            var info2 = $('#password_info');
            var info3 = $('#nickname_info');

            info2.hide();
            info3.hide();

            if(password.length < 6) {
                info2.show();
                isPass = false;
            }
            if(nickname.length < 2) {
                info3.show();
                isPass = false;
            }
            return true;
        }

    </script>
<?php include __DIR__ . '/__html_foot.php' ?>