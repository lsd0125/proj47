<?php
$page_name = 'login';

require __DIR__. '/__connect_db.php';

if(isset($_POST['email_id'])){
    $email_id = $_POST['email_id'];
    $password = $_POST['password'];

    if(!empty($email_id) and !empty($password) ) {
        $password = sha1($password);

        $sql = sprintf("SELECT * FROM `members` WHERE `email_id`='%s' AND `password`='%s'",
            $mysqli->escape_string($email_id),
            $mysqli->escape_string($password)
            );

//        echo $sql;
//        exit;

        $result = $mysqli->query($sql);

        $success = $result->num_rows>0;

        if($success){
            $_SESSION['user'] = $result->fetch_assoc();
        }



/*        $sql = "INSERT INTO `members`(
            `email_id`, `password`, `nickname`, `mobile`,
            `address`, `created_at`, `certification`
            ) VALUES (
            ?, ?, ?, ?,
            ?, NOW(), '$cert'
            )
            ";

        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param('sssss', $email_id, $password, $nickname, $mobile, $address);
        $success = $stmt->execute();
        $stmt->close();*/

    }

}




?>
<?php include __DIR__ . '/__html_head.php' ?>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <?php if(isset($success)): ?>
            <?php if($success): ?>
                <div class="alert alert-success" role="alert">登入成功</div>
            <?php else: ?>
                <div class="alert alert-danger" role="alert">登入失敗</div>
            <?php endif; ?>
        <?php endif; ?>

        <?php if(!isset($success) or $success==false): ?>
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">會員登入</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();"> <!-- 不要讓表單送出 -->
                        <div class="form-group">
                            <label for="email_id">* Email address</label> <span id="email_id_info" style="color:red;display:none;">請填寫正確的 email 格式</span>
                            <input type="email" class="form-control" id="email_id" name="email_id" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">* Password</label> <span id="password_info" style="color:red;display:none;">密碼長度請設定大於 6 !</span>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>

                        <button type="submit" class="btn btn-default">登入</button>
                    </form>
                </div>
            </div>
        </div>

        <?php endif; ?>
    </div>
    <script>
        function checkForm(){
            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var email_id = form1.email_id.value;
            var password = form1.password.value;

            var isPass = true;

            var info1 = $('#email_id_info');
            var info2 = $('#password_info');

            info1.hide();
            info2.hide();

            if(! pattern.test(email_id)) {
                info1.show();
                isPass = false;
            }
            if(password.length < 6) {
                info2.show();
                isPass = false;
            }

            return isPass;
        }

    </script>
<?php include __DIR__ . '/__html_foot.php' ?>