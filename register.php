<?php
$page_name = 'register';

require __DIR__. '/__connect_db.php';

if(isset($_POST['email_id'])){
    $email_id = $_POST['email_id'];
    $password = $_POST['password'];
    $nickname = $_POST['nickname'];
    $mobile = $_POST['mobile'];
    $address = $_POST['address'];

    if(!empty($email_id) and !empty($password) and !empty($nickname) ) {
        $cert = sha1($email_id. uniqid());
        $password = sha1($password);
/*
        $sql = sprintf("INSERT INTO `members`(
            `email_id`, `password`, `nickname`, `mobile`, 
            `address`, `created_at`, `certification`
            ) VALUES (
            '%s', '%s', '%s', '%s',
            '%s', NOW(), '$cert'
            )
            ",
            $mysqli->escape_string($email_id),
            $mysqli->escape_string($password),
            $mysqli->escape_string($nickname),
            $mysqli->escape_string($mobile),
            $mysqli->escape_string($address)
            );

        echo $sql;
        exit;
        */


        $sql = "INSERT INTO `members`(
            `email_id`, `password`, `nickname`, `mobile`, 
            `address`, `created_at`, `certification`
            ) VALUES (
            ?, ?, ?, ?,
            ?, NOW(), '$cert'
            )
            ";

        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param('sssss', $email_id, $password, $nickname, $mobile, $address);
        $success = $stmt->execute();
        $stmt->close();

    }

}




?>
<?php include __DIR__ . '/__html_head.php' ?>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <?php if(isset($success) and $success): ?>
            <div class="alert alert-success" role="alert">註冊成功</div>
        <?php else: ?>
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title">會員註冊</h3></div>
                <div class="panel-body">

                    <form name="form1" method="post" onsubmit="return checkForm();"> <!-- 不要讓表單送出 -->
                        <div class="form-group">
                            <label for="email_id">* Email address</label> <span id="email_id_info" style="color:red;display:none;">請填寫正確的 email 格式</span>
                            <input type="email" class="form-control" id="email_id" name="email_id" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="password">* Password</label> <span id="password_info" style="color:red;display:none;">密碼長度請設定大於 6 !</span>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="nickname">* Nickname</label> <span id="nickname_info" style="color:red;display:none;">暱稱長度請設定大於 2 !</span>
                            <input type="text" class="form-control" id="nickname" name="nickname" placeholder="暱稱">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機號碼">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">註冊</button>
                    </form>


                </div>
            </div>
        </div>

        <?php endif; ?>
    </div>
    <script>
        function checkForm(){
            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var email_id = form1.email_id.value;
            var password = form1.password.value;
            var nickname = form1.nickname.value;
            var pattern2 = /^[a-z]\d{3}/;

            var isPass = true;

            var info1 = $('#email_id_info');
            var info2 = $('#password_info');
            var info3 = $('#nickname_info');
            info1.hide();
            info2.hide();
            info3.hide();

            if(! pattern.test(email_id)) {
                info1.show();
                info1.text('請填寫正確的 email 格式');
                isPass = false;
            }
            if(password.length < 6) {
                info2.show();
                isPass = false;
            }
            if(nickname.length < 2) {
                info3.show();
                isPass = false;
            }

            if(isPass) {
                $.get('aj__check_email_id.php', {email_id: email_id}, function(data){
                    if(data==='yes'){
                        info1.show();
                        info1.text('此 email 已註冊過');
                    } else if(data==='no') {
                        form1.submit();
                    }
                });
            }

            return false;
        }

    </script>
<?php include __DIR__ . '/__html_foot.php' ?>